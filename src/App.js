import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import AddGame from './games/AddGame';
import MultiplicationGame from './games/MultiplicationGame';
import SubstractionGame from './games/SubstractionGame';
import DivisionGame from './games/DivisionGame';
import Sing from "./components/Login/Sing_in_out";
import Register from "./components/Login/register"
import Menu from './components/Menu/Menu';
import Estadisticas from './components/Estadisticas/Estadisticas'

function App() {
  return (
      <Router>
        <Switch>
          <Route exact path="/" component={Sing} />
          <Route exact path="/Regist" component={Register} />
          <Route exact path="/estadisticas" component={Estadisticas} />
          <Route exact path="/menu" component={Menu} />
          <Route exact path="/games/add" component={AddGame} />
          <Route exact path="/games/substraction" component={SubstractionGame} />
          <Route exact path="/games/multiplication" component={MultiplicationGame} />
          <Route exact path="/games/division" component={DivisionGame} />
           
        </Switch>
      </Router>
  );
}

export default App;
