import React, { Fragment, useState, useEffect, useRef } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import firebase, { db } from '../firebase';
import './games.css';

const MultiplicationGame = ({ history }) => {

    //State con información de la operacion y su resultado
    const [multi, setMulti] = useState({
        a: 0,
        b: 0,
        res: 0
    });
    //Destructoring del state 'multi'
    const { a, b, res } = multi;

    //State con opciones de respuesta de los botones
    const [options, setOptions] = useState([]);
    //State con las mismas opciones de 'options' pero mezcladas
    const [shuffledoptions, setShuffledOptions] = useState([]);
    //State con la puntuacion del usuario
    const [score, setScore] = useState(0);
    const scoreRef = useRef(score);
    scoreRef.current = score;

    //Bandera de cuando el usuario responde a la operacion
    const [answered, setAnswerd] = useState(false);
    const [userdata, setUserData] = useState({});
    const userdataRef = useRef(userdata);
    userdataRef.current = userdata;
    const [userid, setUserId] = useState('');
    const useridRef = useRef(userid);
    useridRef.current = userid;

    useEffect(() => {
        const getData = async () => {
            const user = firebase.auth().currentUser;

            if (user) {
                setUserId(user.uid)
                const data = (await db.collection('users').doc(user.uid).get()).data();
                setUserData(data);
            } else {
                history.push('/');
            }
        }
        getData();
        //eslint-disable-next-line
    }, []);

    //Este codigo corre al iniciar el componente
    //Agrega Timer
    useEffect(() => {
        setTimeout(async () => {
            console.log(`id: ${useridRef.current}`);
            alert(`Se agoto tu tiempo, puntuacion: ${scoreRef.current}`);
            if(scoreRef.current > userdataRef.current.maxDiv){
                await db.collection('users').doc(useridRef.current).update({
                    maxDiv: scoreRef.current
                })
            }
            history.push('/');
        }, 30000);
        //eslint-disable-next-line
    }, []);
    
    

    //Este codigo corre cada que el usuario realiza una respuesta
    //Genera una operacion y opciones de respuesta nuevas
    useEffect(() => {
        if (answered) return;
        const generateOperation = () => {
            const a = Math.floor(Math.random() * (100 - 10)) + 10;
            const b = Math.floor(Math.random() * (10 - 1)) + 1;
            const op1 = Math.floor(Math.random() * (100 - 1)) + 1;
            const op2 = Math.floor(Math.random() * (100 - 1)) + 1;
            const res = a / b;
            setMulti({
                a,
                b,
                res
            });
            setOptions([
                {
                    id: 1,
                    value: op1,
                },
                {
                    id: 2,
                    value: op2,
                },
                {
                    id: 3,
                    value: res,
                }
            ]);
        }


        generateOperation();
        setAnswerd(true);

    }, [answered]);

    //Este codigo se ejecuta cada que se generan nuevas opciones de respuesta
    //Mezcla las opciones de respuesta para presentarlas de forma
    //aleatioria en los botones
    useEffect(() => {
        setShuffledOptions(shuffle(options));
    }, [options])

    //Este codigo mezcla un arreglo y te lo regresa desordenado
    const shuffle = array => {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    //Este codigo valida que la respuesta del usuario sea correcta o incorrecta
    const verifyAnswer = value => {
        (value === res) ?
            setScore(score + 1) : console.log("incorrecto");
        setAnswerd(false);
    }


    return (
        <Fragment>
        <div className="marginCard"></div>
        <div className="backgroundGame">
            <Card classname="cardOperation" variant="outlined">
                <CardContent>
                    <Typography color="textSecondary" gutterBottom>
                        Juego de Divisiones
                 </Typography>
                    <Typography variant="h2" component="h2">
                    {a} &#247; {b}  <br />
                        {shuffledoptions.map(option => (
                            <Button
                                variant="contained"
                                color="primary"
                                className="btn"
                                key={option.id}
                                onClick={() => verifyAnswer(option.value)}
                            >
                                {option.value}
                            </Button>
                        ))}
                    </Typography>
                    <Typography color="textSecondary">
                        Piensa Rápido
                </Typography>
                    <Typography variant="body2" component="p">
                        Puntuación actual: {score}
                        <br />
                        {`Record: ${userdata.maxDiv}`}
                    </Typography>
                </CardContent>
                {/* <CardActions>
                    <Button size="small">Cancelar</Button>
                </CardActions> */}
            </Card>

        </div>
    </Fragment>
    );
}

export default MultiplicationGame;