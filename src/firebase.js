import firebase from 'firebase';

var firebaseConfig = {
    apiKey: "AIzaSyBzDfMMzhPy6us5yzL4Tn6QtjrKAyX1TvI",
    authDomain: "school-app-8ac74.firebaseapp.com",
    databaseURL: "https://school-app-8ac74.firebaseio.com",
    projectId: "school-app-8ac74",
    storageBucket: "school-app-8ac74.appspot.com",
    messagingSenderId: "633230816570",
    appId: "1:633230816570:web:be6e7b738eff27b4658c03"
};
// Initialize Firebase
var firebaseInit = firebase.initializeApp(firebaseConfig);
export var db = firebase.firestore(firebaseInit);
export default firebase;



