import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import HomeImage from '../../assets/images/kids2.jpg';
import { useHistory } from 'react-router-dom';
import './menu.css';
import firebase, { db } from '../../firebase';

const useStyles = makeStyles(theme => ({
    font1: {
        display: "flex",
        flexDirection: "row",
        width: "100%",
        height: "100vh",
        overflowY: "hidden"
    },
    menu: {
        width: "20%",
        height: "100%",
        flexDirection: "column",
        backgroundColor: "#6E00FF",
        color: "white",
        fontWeight: "bold"
    },
    view: {
        backgroundImage: `url(${HomeImage})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",

        justifyContent: "center",
        width: "100%",
        height: "100%"

    }
}));

export default function Menu() {
    const classes = useStyles();
    const history = useHistory();
    const [userdata, setUserData] = useState({});

    useEffect(() => {
        const getData = async () => {
            const user = firebase.auth().currentUser;

            if (user) {
                const data = (await db.collection('users').doc(user.uid).get()).data();
                setUserData(data);
            } else {
                history.push('/');
            }
        }
        getData();
        //eslint-disable-next-line
    }, []);


    const handleCloseSession = () => {
        firebase.auth().signOut().then(function () {
            history.push('/')
        }).catch(function (error) {
            console.log(error);
        });

    }

    return (
        <div className={classes.font1}>
            <div className={classes.menu}>
                <div className="user"> {userdata.username} </div>
                <div className="btn-juegos" type="button" onClick={e => history.push("/menu")}>JUEGOS</div>
                <div className="btn-estadisticas" type="button" onClick={e => history.push("/estadisticas")}> ESTADISTICAS</div>
                <div className="cerrar-sesion" type="button" onClick={handleCloseSession}> CERRAR SESION</div>
            </div>
            <div className={classes.view}>
                <div className="contenedor1">
                    <div className="container">
                        <div className="row">
                            <div className="col-4">
                                <div className="sumas" type="button" onClick={e => history.push("/games/add")}> + </div>
                            </div>
                            <div className="col-4" >
                                <div className="restas" type="button" onClick={e => history.push("/games/substraction")}> - </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-4" >
                                <div className="multi" type="button" onClick={e => history.push("/games/multiplication")}> x </div>
                            </div>
                            <div className="col-4">
                                <div className="divi" type="button" onClick={e => history.push("/games/division")}> &#247; </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}