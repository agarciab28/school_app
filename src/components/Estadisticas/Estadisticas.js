import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import HomeImage from '../../assets/images/kids3.jpg';
import Resultados from '../Box-Resultados/resultados'
import { useHistory } from 'react-router-dom';
import 'date-fns';
import '../Menu/menu.css';
import firebase, { db } from '../../firebase';

const useStyles = makeStyles(theme => ({
    font1: {
        display: "flex",
        flexDirection: "row",
        width: "100%",
        height: "100vh",
        overflowY: "hidden"
    },
    menu: {
        width: "20%",
        height: "100%",
        flexDirection: "column",
        backgroundColor: "#6E00FF",
        color: "white",
        fontWeight: "bold"
    },
    view: {
        backgroundImage: `url(${HomeImage})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        alignContent: "center",
        justifyContent: "center",
        width: "100%",
        height: "100%"
    }
}));

export default function Login() {
    const classes = useStyles();
    const history = useHistory();
    const [userdata, setUserData] = useState({});
    useEffect(() => {
        const getData = async () => {
            const user = firebase.auth().currentUser;

            if (user) {
                const data = (await db.collection('users').doc(user.uid).get()).data();
                setUserData(data);
            } else {
                history.push('/');
            }
        }
        getData();
        //eslint-disable-next-line
    }, []);
    // var f = new Date();
    // var [selectedDate, setSelectedDate] = React.useState(new Date(f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate()));
    // var fechaselect;
    // const handleDateChange = date => {
    //     setSelectedDate(date);
    // };

    // const handleformato = () => {
    //     var nuevo;

    //     var arraydate = selectedDate + "";
    //     var nuevo = arraydate.split(" ");

    //     fechaselect = nuevo[2];
    //     switch (nuevo[1]) {
    //         case 'Jan': fechaselect += "/01/";
    //             break;
    //         case 'Feb': fechaselect += "/02/";
    //             break;
    //         case 'Mar': fechaselect += "/03/";
    //             break;
    //         case 'Apr': fechaselect += "/04/";
    //             break;
    //         case 'May': fechaselect += "/05/";
    //             break;
    //         case 'Jun': fechaselect += "/06/";
    //             break;
    //         case 'Jul': fechaselect += "/07/";
    //             break;
    //         case 'Aug': fechaselect += "/08/";
    //             break;
    //         case 'Sep': fechaselect += "/09/";
    //             break;
    //         case 'Oct': fechaselect += "/10/";
    //             break;
    //         case 'Nov': fechaselect += "/11/";
    //             break;
    //         case 'Dec': fechaselect += "/12/";
    //             break;
    //     }
    //     fechaselect += nuevo[3];

    // }

    return (
        <div className={classes.font1}>
            <div className={classes.menu}>
                <div className="user"> {userdata.username}</div>
                <div className="btn-juegos" type="button" onClick={e => history.push("/menu")}>JUEGOS</div>
                <div className="btn-estadisticas" type="button" onClick={e => history.push("/estadisticas")}> ESTADISTICAS</div>
                <div className="cerrar-sesion" type="button" onClick={e => history.push("/")}> CERRAR SESION</div>
            </div>
            <div className={classes.view}>
                <Resultados
                    sumas={userdata.maxAdd}
                    restas={userdata.maxSub}
                    multiplicacion={userdata.maxMulti}
                    division={userdata.maxDiv}
                />
            </div>
        </div>
    );
}



