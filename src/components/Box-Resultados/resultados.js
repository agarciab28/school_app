import React from 'react'
import TextField from "@material-ui/core/TextField";
import './resultados.css'

const Resultados = ({sumas, restas, multiplicacion, division}) =>{
    
    return(
        <div className="resultados">
            <div className="title">  RESULTADOS</div>
           <table className="table table-striped">
               <tr>
                  <th scope="row"><div className="title-def">SUMAS: </div></th>
                  <th><TextField
                  margin="normal"
                  name="Sumas"
                  value={sumas}
                  disabled={true}
                  /></th>
                </tr>
                <tr>
                  <th scope="row"><div className="title-def">RESTAS: </div></th>
                  <th><TextField
                  margin="normal"
                  name="Restas"
                  value={restas}
                  disabled={true}
                  /></th>
                </tr> 
                <tr>
                    <th scope="row"><div className="title-def">MULTIPLICACIONES: </div></th>
                    <th><TextField
                    margin="normal"
                    name="Multipli"
                    value={multiplicacion}
                    disabled={true}
                    /></th>
                </tr>
                <tr>
                  <th scope="row"><div className="title-def">DIVISIONES: </div></th>
                  <th><TextField
                  margin="normal"
                  name="Division"
                  value={division}
                  disabled={true}
                  /></th>
                </tr>
            </table>
        </div>
    )
}

export default Resultados;