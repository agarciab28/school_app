import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import clsx from 'clsx';
import HomeImage from '../../assets/images/kids1.jpg';
import Logo from '../../assets/images/kids1.jpg';
import firebase, { db } from '../../firebase';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "row",
        width: "100%",
        height: "100vh",
    },
    izquierda: {
        width: "50%",
        backgroundImage: `linear-gradient(
            rgba(200,200,200, .5),
            rgba(200,200,200, .5)
          ),url(${HomeImage})`,
        height: "100%",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        alignContent: "center",
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
        [theme.breakpoints.down(750)]: {
            display: "none",
        }
    },
    derecha: {
        width: "50%",
        backgroundColor: "white",
        height: "100%",
        alignContent: "center",
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
        [theme.breakpoints.down(750)]: {
            width: "100%",
        }
    },
    margin: {
        margin: "auto",
        width: "100%",
        alignSelf: "center"
    },
    imgres: {
        display: "none",
        width: "25%",
        alignSelf: "center",
        [theme.breakpoints.down(750)]: {
            display: "block",
        }
    },
    btnres: {
        backgroundColor: "#FFC107",
        color: "white",
        marginTop: "1vh",
        width: "40%",
        alignSelf: "center",
        marginBottom: "8vh",
        display: "none",
        [theme.breakpoints.down(750)]: {
            display: "block",
        }
    },
    formulario: {
        width: "100%",
        display: "flex",
        marginLeft: "50%",
        alignSelf: "center",
        [theme.breakpoints.down(750)]: {
            width: "80%",
        }
    },
    contenedor1: {
        backgroundColor: "black",
        opacity: "0.5",
        alignContent: "center",
        justifyContent: "center",
        width: "70%",
        marginLeft: "20%"
    },
    btnsesion: {
        backgroundColor: "#2185D0",
        color: "white",
        marginTop: "1vh",
        width: "50%",
        alignSelf: "center",
        marginBottom: "8vh",
        marginLeft: "25%"
    },
    title: {
        width: "100%",
        backgroundColor: "#6E00FF",
        color: "white",
        height: "10%",
        alignContent: "center",
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
        [theme.breakpoints.down(750)]: {
            width: "100%",
        }
    },
    font1: {
        display: "flex",
        flexDirection: "column",
        width: "100%",
        height: "100vh",
        overflowY: "hidden"
    }
}));

export default function Register() {
    const classes = useStyles();
    const [values, setValues] = useState({
        nombre: '',
        password: '',
        confirm: '',
        email: '',
        username: '',
        showPassword: false,
    });
    const history = useHistory();
    const handleChange = prop => event => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = event => {
        event.preventDefault();
    };
    const handleSubmit = e => {
        e.preventDefault();
        if(values.nombre.trim() === '' || values.password.trim() === '' || values.confirm.trim() === '' || values.email.trim() === ''){
            alert('Todos los campos necesitan ser llenados');
            return;
        }
        if(values.password.trim() !== values.confirm.trim()){
            alert('Las contraseñas no coinciden');
            return;
        }

        if(values.password.length < 6){
            alert('Contraseña debe ser mínimo de 6 cáracteres');
            return;
        }

        firebase.auth().createUserWithEmailAndPassword(values.email, values.password).then(() => {
            var userId = firebase.auth().currentUser.uid;
            db.collection("users").doc(userId).set({
                name: values.nombre,
                email: values.email,
                username: values.username,
                maxAdd: '0',
                maxSub: '0',
                maxDiv: '0',
                maxMulti: '0'
            })
            .then(function() {
                console.log("Document successfully written!");
            })
            .catch(function(error) {
                console.error("Error writing document: ", error);
            });
            // firebase.auth().signInWithEmailAndPassword(values.email, values.password).then(() =>{
                
            // });
        })
          alert('Usuario creado correctamente');
          history.push('/');
    }
    return (
        <div className={classes.font1}>
            <div className={classes.title}>
                <Typography style={{ fontSize: "large", textAlign: "center", color: "white", marginTop: "1svh" }}>
                    APRENDE RAPIDO Y FACIL CON LINKER-MATH
            </Typography>
            </div>
            <div className={classes.root}>
                <div className={classes.izquierda}>
                    <div className={classes.contenedor1}>
                        <Typography style={{ fontSize: "large", textAlign: "center", color: "white", marginTop: "8vh" }}>
                            ¿Ya tienes cuenta?<br />
                    Inicia Sesión ahora mismo

                </Typography>
                        <Button variant="contained" className={classes.btnsesion} onClick={e => history.push("/")} >
                            Iniciar Sesión
                </Button>
                    </div>
                </div>
                <div className={classes.derecha}>
                    <img src={Logo} alt="" className={classes.imgres} />
                    <Typography style={{ fontWeight: "bold", fontSize: "larger", textAlign: "center" }}>
                        Registrar
             </Typography>
                    <Typography style={{
                        fontSize: "large",
                        textAlign: "center",
                        marginBottom: "4vh",
                    }}
                        color="textSecondary">
                        Ingresa los siguientes datos para crear una cuenta
             </Typography>
                    <form className={classes.formulario} onSubmit={handleSubmit} id="formulario">
                        <div style={{ width: "50%", display: "flex", flexDirection: "column", marginRight: "2%" }}>
                            <TextField
                                label="Nombre Completo"
                                helperText="Ingresa Nombre"
                                variant="filled"
                                style={{
                                    width: "100%",
                                    alignSelf: "center",
                                    marginBottom: "2%"
                                }}
                                onChange={handleChange('nombre')}
                            />
                            <TextField
                                label="Nombre de Usuario"
                                helperText="Ingresa nombre de usuario"
                                variant="filled"
                                style={{
                                    width: "100%",
                                    alignSelf: "center",
                                    marginBottom: "2%"
                                }}
                                onChange={handleChange('username')}
                            />

                            <TextField
                                label="Correo electrónico"
                                type="email"
                                helperText="Ingresa correo electrónico"
                                variant="filled"
                                style={{
                                    width: "100%",
                                    alignSelf: "center",
                                    marginBottom: "2%"
                                }}
                                onChange={handleChange('email')}
                            />

                            <FormControl className={clsx(classes.margin, classes.textField)} variant="filled">
                                <InputLabel htmlFor="filled-adornment-password">Password</InputLabel>
                                <FilledInput
                                    type={values.showPassword ? 'text' : 'password'}
                                    value={values.password}
                                    onChange={handleChange('password')}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={handleClickShowPassword}
                                                onMouseDown={handleMouseDownPassword}
                                                edge="end"
                                            >
                                                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                    }
                                />
                                <FormHelperText id="standard-weight-helper-text">Ingresa la contraseña</FormHelperText>
                            </FormControl>

                            <FormControl className={clsx(classes.margin, classes.textField)} variant="filled">
                                <InputLabel htmlFor="filled-adornment-password">Password</InputLabel>
                                <FilledInput
                                    type={values.showPassword ? 'text' : 'password'}
                                    value={values.confirm}
                                    onChange={handleChange('confirm')}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={handleClickShowPassword}
                                                onMouseDown={handleMouseDownPassword}
                                                edge="end"
                                            >
                                                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                    }
                                />
                                <FormHelperText id="standard-weight-helper-text">Comprueba la contraseña</FormHelperText>
                            </FormControl>

                        </div>
                    </form>
                    <Button variant="contained" form="formulario" type="submit" style={{ backgroundColor: "#2185D0", color: "white", marginTop: "1vh", width: "40%", alignSelf: "center" }} >
                        Registrar
                 </Button>
                    <Button variant="contained" className={classes.btnres} onClick={e => history.push("/")}>
                        Iniciar Sesión
                </Button>
                </div>
            </div>
        </div>
    );
}