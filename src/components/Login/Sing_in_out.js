import React, { useState, useEffect } from 'react';
import {useHistory} from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import clsx from 'clsx';
import HomeImage from '../../assets/images/kids1.jpg';
import Logo from '../../assets/images/kids.jpg';
import firebase from '../../firebase';

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: "row",
        width: "100%",
        height: "100vh",
    },
    font1:{
        display: "flex",
        flexDirection: "column",
        width: "100%",
        height: "100vh",
        overflowY:"hidden"
    },
    izquierda: {
        width: "50%",
        backgroundImage: `linear-gradient(
            rgba(200,200,200, .5),
            rgba(200,200,200, .5)
          ),url(${HomeImage})`,
        height: "100%",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        alignContent: "center",
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
        [theme.breakpoints.down(750)]:{
            display:"none",
        }
    },
    derecha: {
        width: "50%",
        backgroundColor: "white",
        height: "100%",
        alignContent: "center",
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
        [theme.breakpoints.down(750)]:{
            width:"100%",
        }
    },
    margin: {
        margin: "auto",
        width: "100%",
        marginTop: "4%"
    },
    imgres:{
        display:"none",
        width:"25%",
        alignSelf: "center",
        [theme.breakpoints.down(750)]:{
            display:"block",
        }
    },
    btnres:{
        backgroundColor:"#2185D0",
        color:"white",
        marginTop:"1vh",
        width:"100%",
        alignSelf:"center",
        marginBottom:"8vh",
        display:"none", 
        [theme.breakpoints.down(750)]:{
            display:"block",
        } 
    },
    title:{
        width: "100%",
        backgroundColor:"#6E00FF",
        color:"white",
        height: "10%",
        alignContent: "center",
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
        [theme.breakpoints.down(750)]:{
            width:"100%",
        }
    },
    contenedor1:{
        backgroundColor:"black",
        opacity:"0.5",
        alignContent: "center",
        justifyContent: "center",
        width: "70%",
        marginLeft:"20%"
    },
    btncrear:{
        backgroundColor:"#2185D0",
        color:"white",
        marginTop:"1vh",
        width:"50%",
        alignSelf:"center",
        marginBottom:"8vh",
        marginLeft:"25%",
        [theme.breakpoints.down(750)]:{
            backgroundColor:"#2185D0"
        }
    },
}));

export default function Login() {
    const classes = useStyles();
    const history = useHistory();
    const [values, setValues] = useState({
        password: '',
        email: '',
        showPassword: false,
    });
    const [login, setLogin] = useState(false);

    useEffect(() => {
        const user = firebase.auth().currentUser;

        if(user){
            history.push('/menu')
        } else {
            setLogin(false);
        }
          //eslint-disable-next-line
    }, [login])

    useEffect(() => {
        const user = firebase.auth().currentUser;

        if(user){
            history.push('/menu')
        } else {
            setLogin(false);
        }
          //eslint-disable-next-line
    }, [])

    const handleChange = prop => event => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = event => {
        event.preventDefault();
    };
    const handleSubmit = e => {
        e.preventDefault();
        if(values.email.trim() === '' || values.password.trim() === ''){
            alert('Todos los campos son obligatorios');
            return;
        }
        firebase.auth().signInWithEmailAndPassword(values.email, values.password).catch(function(error) {
            alert('No se ha encontrado el usuario, intente crear uno nuevo');
            return;
            // ...
          });
          setLogin(true);
    }
    return (
        <div  className={classes.font1}>
            <div  className={classes.title}>
            <Typography style={{ fontSize: "large", textAlign: "center",color:"white",marginTop:"1svh"}}>
                   APRENDE RAPIDO Y FACIL CON LINKER-MATH
            </Typography>
            </div>
        <div className={classes.root}>
            
            <div className={classes.izquierda}>
                
                <div className={classes.contenedor1}>
                <Typography style={{ fontSize: "large", textAlign: "center",color:"white",marginTop:"8vh"}}>
                    ¿No tienes cuenta?<br/>
                    Registrate es gratis y lo seguira siendo.
                </Typography>
                <Button variant="contained" className={classes.btncrear} onClick={e =>history.push("/Regist")}>
                        Crear cuenta
                </Button>
                </div>
            </div>
            <div className={classes.derecha}>
            <img src={Logo} alt="" className={classes.imgres}/>
                <Typography style={{ fontWeight: "bold", fontSize: "larger", textAlign: "center" }}>
                    Iniciar Sesión
             </Typography>
                <Typography style={{
                    fontSize: "large",
                    textAlign: "center",
                    marginBottom: "4vh",
                }}
                    color="textSecondary">
                    Ingresa tus credenciales para iniciar sesión
             </Typography>
                <form style={{
                    width: "60%",
                    alignSelf: "center",
                }}
                    onSubmit={handleSubmit}>
                    <TextField
                        id="filled-helperText"
                        label="Correo electrónico"
                        helperText="Ingresa correo electrónico"
                        variant="filled"
                        style={{
                            width: "100%",
                            alignSelf: "center",
                            marginTop: "4vh"
                        }}
                        onChange={handleChange('email')}
                    />
                    <FormControl className={clsx(classes.margin, classes.textField)} variant="filled">
                        <InputLabel htmlFor="filled-adornment-password">Password</InputLabel>
                        <FilledInput
                            id="filled-adornment-password"
                            type={values.showPassword ? 'text' : 'password'}
                            value={values.password}
                            onChange={handleChange('password')}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                        edge="end"
                                    >
                                        {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                        <FormHelperText id="standard-weight-helper-text">Ingresa la contraseña</FormHelperText>
                    </FormControl>
                    <Button variant="contained" type="submit" style={{backgroundColor:"#2185D0",color:"white",marginTop:"1vh",width:"100%"}}>
                        Login
                    </Button>
                    <Button variant="contained" className={classes.btnres}  x onClick={e =>history.push("/Regist")}>
                        Crear cuenta
                    </Button>
                </form>
            </div>
        </div>
        </div>
    );
}